import React from 'react';

export default class FetchRandomUser extends React.Component {

    constructor (props) {
        super(props)
        this.state = {loading: true,
            person: null};
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(this.props.reload)
        if(this.props.reload && prevProps.reload !== this.props.reload) {
            await this.callApi();
        }
    }

    async callApi() {
        this.setState({loading : true})
        const url = "https://api.randomuser.me/"
        const response = await fetch(url)
        const data = await response.json()
        this.setState({person: data.results[0], loading: false})
    }

    render() {
        if(this.state.loading) {
            return <div>loading...</div>
        } else if(!this.state.person) {
            return <div>didn't get</div>
        } else {
            return (
            <div>
                <div>{this.state.person.name.first}</div>
                <div>{this.state.person.name.last}</div>
                <div>{this.state.person.name.title}</div>
                <img src={this.state.person.picture.large}/>
            </div>
            );
        }
        
        
    }
}

